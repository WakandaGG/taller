import sys
import os
from fractions import Fraction
from tkinter import*
from tkinter import messagebox
from math import*

def ConvVol():
	vent_conv = Toplevel(ventana)
	vent_conv.geometry("350x200")

	Label(vent_conv,text="De :").place(x=50,y=10)
	Radiobutton(vent_conv,text="m3",variable =Volumen, value="m3").place(x=50,y=40)
	Radiobutton(vent_conv,text="cm3",variable =Volumen, value="cm3").place(x=50,y=60)
	Radiobutton(vent_conv,text="Litros",variable =Volumen, value="lts").place(x=50,y=80)
	Radiobutton(vent_conv,text="Galones",variable =Volumen, value="galones").place(x=50,y=100)

	Label(vent_conv,text="Transformar a :").place(x=200,y=10)
	Radiobutton(vent_conv,text="m3",variable =Volumen2, value="m3").place(x=200,y=40)
	Radiobutton(vent_conv,text="cm3",variable =Volumen2, value="cm3").place(x=200,y=60)
	Radiobutton(vent_conv,text="Litros",variable =Volumen2, value="lts").place(x=200,y=80)
	Radiobutton(vent_conv,text="Galones",variable =Volumen2, value="galones").place(x=200,y=100)

	Entry(vent_conv,textvariable=vol1,width=10).place(x=50,y=140)

	Button(vent_conv, text="Procesar", command=ProcesarVol).place(x=250,y=140,width=100)
	vent_conv.mainloop()

def ProcesarVol():
	if Volumen.get()=="m3":
		if Volumen2.get()=="m3":
			messagebox.showinfo("", "RESULTADO:"+ "" + str(vol1.get()) + Volumen2.get())
		elif Volumen2.get()=="cm3":
			r=vol1.get()*1000000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="lts":
			r=vol1.get()/1000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="galones":
			r=vol1.get()/264172
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())

	elif Volumen.get()=="cm3":
		if Volumen2.get()=="cm3":
			messagebox.showinfo("", "RESULTADO:" + "" + str(vol1.get()) + Volumen2.get())
		elif Volumen2.get()=="m3":
			r=vol1.get()/0.000001
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="lts":
			r=vol1.get()/0.001
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="galones":
			r=vol1.get()/0.000264172
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())

	elif Volumen.get()=="lts":
		if Volumen2.get()=="lts":
			messagebox.showinfo("", "RESULTADO:" + "" + str(vol1.get()) + Volumen2.get())
		elif Volumen2.get()=="m3":
			r=vol1.get()*0.001
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="cm3":
			r=vol1.get()*1000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="galones":
			r=vol1.get()/0.264172
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
	
	elif Volumen.get()=="galones":
		if Volumen2.get()=="galones":
			messagebox.showinfo("", "RESULTADO:" + "" + str(vol1.get()) + Volumen2.get())
		elif Volumen2.get()=="m3":
			r=vol1.get()*0.00378541
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="cm3":
			r=vol1.get()*3785.41
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())
		elif Volumen2.get()=="lts":
			r=vol1.get()*378541
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Volumen2.get())

def ConvLong():
	vent_conv = Toplevel(ventana)
	vent_conv.geometry("350x200")

	Label(vent_conv,text="De :").place(x=50,y=10)
	Radiobutton(vent_conv,text="mm",variable =Longitud, value="mm").place(x=50,y=40)
	Radiobutton(vent_conv,text="cms",variable =Longitud, value="cms").place(x=50,y=60)
	Radiobutton(vent_conv,text="mts",variable =Longitud, value="mts").place(x=50,y=80)
	Radiobutton(vent_conv,text="kms",variable =Longitud, value="kms").place(x=50,y=100)
	Radiobutton(vent_conv,text="Pulgadas",variable =Longitud, value="plg").place(x=50,y=120)

	Label(vent_conv,text="Transformar a :").place(x=200,y=10)
	Radiobutton(vent_conv,text="mm",variable =Longitud2, value="mm").place(x=200,y=40)
	Radiobutton(vent_conv,text="cms",variable =Longitud2, value="cms").place(x=200,y=60)
	Radiobutton(vent_conv,text="mts",variable =Longitud2, value="mts").place(x=200,y=80)
	Radiobutton(vent_conv,text="kms",variable =Longitud, value="kms").place(x=200,y=100)
	Radiobutton(vent_conv,text="Pulgadas",variable =Longitud2, value="plg").place(x=200,y=120)

	Entry(vent_conv,textvariable=long1,width=10).place(x=50,y=140)

	Button(vent_conv, text="Procesar", command=ProcesarLon).place(x=200,y=140,width=100)
	vent_conv.mainloop()

def ProcesarLon():
	if Longitud.get()=="mm":
		if Longitud2.get()=="mm":
			messagebox.showinfo("", "RESULTADO:" + "" + str(long1.get()) + Longitud2.get())
		elif Longitud2.get()=="cms":
			r=long1.get()/10
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="mts":
			r=long1.get()/1000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="kms":
			r=long1.get()/1000000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="plg":
			r=long1.get()*0.0393701
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())

	elif Longitud.get()=="cms":
		if Longitud2.get()=="cms":
			messagebox.showinfo("", "RESULTADO:" + "" + str(long1.get()) + Longitud2.get())
		elif Longitud2.get()=="mm":
			r=long1.get()*0.1
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="mts":
			r=long1.get()/100
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="kms":
			r=long1.get()/100000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="plg":
			r=long1.get()*0.393701
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())

	elif Longitud.get()=="mts":
		if Longitud2.get()=="mts":
			messagebox.showinfo("", "RESULTADO:" + "" + str(long1.get()) + Longitud2.get())
		elif Longitud2.get()=="mm":
			r=long1.get()*1000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="cms":
			r=long1.get()*100
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="kms":
			r=long1.get()/0.1
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="plg":
			r=long1.get()*393701
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())

	elif Longitud.get()=="kms":
		if Longitud2.get()=="kms":
			messagebox.showinfo("", "RESULTADO:" + "" + str(long1.get()) + Longitud2.get())
		elif Longitud2.get()=="mm":
			r=long1.get()*1000000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="cms":
			r=long1.get()*100000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="mts":
			r=long1.get()*1000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="plg":
			r=long1.get()*39370.1
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		
	elif Longitud.get()=="plg":
		if Longitud2.get()=="plg":
			messagebox.showinfo("", "RESULTADO:" + "" + str(long1.get()) + Longitud2.get())
		elif Longitud2.get()=="mm":
			r=long1.get()/25.4
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="cms":
			r=long1.get()/2.54
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="mts":
			r=long1.get()/0.0254
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())
		elif Longitud2.get()=="kms":
			r=long1.get()/0.0000254
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Longitud2.get())

def ConvTemp():
	vent_conv = Toplevel(ventana)
	vent_conv.geometry("350x200")

	Label(vent_conv,text="De :").place(x=50,y=10)
	Radiobutton(vent_conv,text="C",variable =Temperatura, value="c").place(x=50,y=40)
	Radiobutton(vent_conv,text="F",variable =Temperatura, value="f").place(x=50,y=60)
	Radiobutton(vent_conv,text="K",variable =Temperatura, value="k").place(x=50,y=80)

	Label(vent_conv,text="Transformar a :").place(x=200,y=10)
	Radiobutton(vent_conv,text="C",variable =Temperatura2, value="c").place(x=200,y=40)
	Radiobutton(vent_conv,text="F",variable =Temperatura2, value="f").place(x=200,y=60)
	Radiobutton(vent_conv,text="K",variable =Temperatura2, value="k").place(x=200,y=80)

	Entry(vent_conv,textvariable=temp1,width=10).place(x=50,y=140)

	Button(vent_conv, text="Procesar", command=ProcesarTemp).place(x=200,y=140,width=100)
	vent_conv.mainloop()

def ProcesarTemp():
	if Temperatura.get()=="C":
		if Temperatura2.get()=="C":
			messagebox.showinfo("", "RESULTADO:" + "" + str(temp1.get()) + Temperatura2.get())
		elif Temperatura2.get()=="F":
			r=(temp1.get()* 9/5)+32
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Temperatura2.get())
		elif Temperatura2.get()=="K":
			r=temp1.get()+273.15
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Temperatura2.get())

	elif Temperatura.get()=="F":
		if Temperatura2.get()=="F":
			messagebox.showinfo("", "RESULTADO:" + "" + str(temp1.get()) + Temperatura2.get())
		elif Temperatura2.get()=="C":
			r=(temp1.get()- 32) * 5/9
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Temperatura2.get())
		elif Temperatura2.get()=="K":
			r=(temp1.get()- 32) * 5/9 + 273.15
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Temperatura2.get())

	elif Temperatura.get()=="K":
		if Temperatura2.get()=="K":
			messagebox.showinfo("", "RESULTADO:" + "" + str(temp1.get()) + Temperatura2.get())
		elif Temperatura2.get()=="C":
			r=temp1.get()-273.15
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Temperatura2.get())
		elif Temperatura2.get()=="F":
			r=(temp1.get()- 273.15) * 9/5 + 32  
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Temperatura2.get())

def ConvPeso():
	vent_conv = Toplevel(ventana)
	vent_conv.geometry("350x200")

	Label(vent_conv,text="De :").place(x=50,y=10)
	Radiobutton(vent_conv,text="Gramos",variable =Peso, value="grs").place(x=50,y=40)
	Radiobutton(vent_conv,text="Kilogramos",variable =Peso, value="kg").place(x=50,y=60)
	Radiobutton(vent_conv,text="Toneladas",variable =Peso, value="ton").place(x=50,y=80)
	Radiobutton(vent_conv,text="Libras",variable =Peso, value="lb").place(x=50,y=100)

	Label(vent_conv,text="Transformar a :").place(x=200,y=10)
	Radiobutton(vent_conv,text="Gramos",variable =Peso2, value="grs").place(x=200,y=40)
	Radiobutton(vent_conv,text="Kilogramos",variable =Peso2, value="kg").place(x=200,y=60)
	Radiobutton(vent_conv,text="Toneladas",variable =Peso2, value="ton").place(x=200,y=80)
	Radiobutton(vent_conv,text="Libras",variable =Peso2, value="lb").place(x=200,y=100)

	Entry(vent_conv,textvariable=peso1,width=10).place(x=50,y=140)

	Button(vent_conv, text="Procesar", command=ProcesarPeso).place(x=200,y=140,width=100)
	vent_conv.mainloop()

def ProcesarPeso():
	if Peso.get()=="grs":
		if Peso2.get()=="grs":
			messagebox.showinfo("", "RESULTADO:" + "" + str(pres1.get()) + Peso2.get())
		elif Peso2.get()=="kg":
			r=peso1.get()/0.01
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="ton":
			r=peso1.get()/0.110231
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="lb":
			r=peso1.get()/0.00220462
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())

	elif Peso.get()=="kg":
		if Peso2.get()=="kg":
			messagebox.showinfo("", "RESULTADO:" + "" + str(pres1.get()) + Peso2.get())
		elif Peso2.get()=="grs":
			r=peso1.get()*1000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="ton":
			r=peso1.get()/0.001
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="lb":
			r=peso1.get()/2
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())

	elif Peso.get()=="ton":
		if Peso2.get()=="ton":
			messagebox.showinfo("", "RESULTADO:" + "" + str(pres1.get()) + Peso2.get())
		elif Peso2.get()=="grs":
			r=peso1.get()*1000000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="kg":
			r=peso1.get()*1000
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="lb":
			r=peso1.get()/2204.62
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())

	elif Peso.get()=="lb":
		if Peso2.get()=="lb":
			messagebox.showinfo("", "RESULTADO:" + "" + str(pres1.get()) + Peso2.get())
		elif Peso2.get()=="grs":
			r=peso1.get()*453592
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="kg":
			r=peso1.get()*0.453592
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())
		elif Peso2.get()=="ton":
			r=peso1.get()*0.000453592
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Peso2.get())

def ConvPresion():
	vent_conv = Toplevel(ventana)
	vent_conv.geometry("350x200")

	Label(vent_conv,text="De :").place(x=50,y=10)
	Radiobutton(vent_conv,text="ATM",variable =Presion, value="atm").place(x=50,y=40)
	Radiobutton(vent_conv,text="PA",variable =Presion, value="pa").place(x=50,y=60)
	Radiobutton(vent_conv,text="mmHg",variable =Presion, value="mmhg").place(x=50,y=80)

	Label(vent_conv,text="Transformar a :").place(x=200,y=10)
	Radiobutton(vent_conv,text="ATM",variable =Presion2, value="atm").place(x=200,y=40)
	Radiobutton(vent_conv,text="PA",variable =Presion2, value="pa").place(x=200,y=60)
	Radiobutton(vent_conv,text="mmHg",variable =Presion2, value="mmhg").place(x=200,y=80)

	Entry(vent_conv,textvariable=pres1,width=10).place(x=50,y=140)

	Button(vent_conv, text="Procesar", command=ProcesarPres).place(x=200,y=140,width=100)

def ProcesarPres():
	if Presion.get()=="atm":
		if Presion2.get()=="atm":
			messagebox.showinfo("", "RESULTADO:" + "" + str(pres1.get()) + Presion2.get())
		elif Presion2.get()=="pa":
			r=pres1.get()*101325
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Presion2.get())
		elif Presion2.get()=="mmhg":
			r=pres1.get()/760
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Presion2.get())

	elif Presion.get()=="pa":
		if Presion2.get()=="pa":
			messagebox.showinfo("", "RESULTADO:" + "" + str(pres1.get()) + Presion2.get())
		elif Presion2.get()=="atm":
			r=pres1.get()/0.098692
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Presion2.get())
		elif Presion2.get()=="mmhg":
			r=pres1.get()/0.00750062
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Presion2.get())

	elif Presion.get()=="mmhg":
		if Presion2.get()=="mmhg":
			messagebox.showinfo("", "RESULTADO:" + "" + str(pres1.get()) + Presion2.get())
		elif Presion2.get()=="atm":
			r=pres1.get()*0.00131579
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Presion2.get())
		elif Presion2.get()=="pa":
			r=pres1.get()*133322
			messagebox.showinfo("", "RESULTADO:" + "" + str(r) + Presion2.get())

def salir():
    if messagebox.askyesno("","Desea Salir?"):
        ventana.destroy()

ventana=Tk()
ventana.title("Calculadora Cientifica")
ventana.geometry("450x500")
a=StringVar()

#Guardar
guardado1=StringVar()
guardado2=StringVar()
guardado3=StringVar()
guardado4=StringVar()
guardado5=StringVar()
#VOLUMEN
Volumen = StringVar()
Volumen.set("m3")
Volumen2 = StringVar()
Volumen2.set("m3")
vol1=IntVar()
#LONGITUD
Longitud = StringVar()
Longitud.set("mts")
Longitud2 = StringVar()
Longitud2.set("mts")
long1=IntVar()
#TEMPERATURA
Temperatura = StringVar()
Temperatura.set("c")
Temperatura2 = StringVar()
Temperatura2.set("c")
temp1=IntVar()
#PESO
Peso = StringVar()
Peso.set("atm")
Peso2 = StringVar()
Peso2.set("atm")
peso1=IntVar()
#PRESION
Presion = StringVar()
Presion.set("atm")
Presion2 = StringVar()
Presion2.set("atm")
pres1=IntVar()

screen=""

def refreshscreen(num):
	global screen
	screen=screen+str(num)
	a.set(screen)

def clearScreenC():
	global screen
	screen=" "
	a.set(screen)

def EvaluateOperation():
	global screen
	op=eval(screen)
	a.set(op)

def Guardar1():
	guardado1.set(a.get())
def Guardar2():
	guardado2.set(a.get())
def Guardar3():
	guardado3.set(a.get())
def Guardar4():
	guardado4.set(a.get())
def Guardar5():
	guardado5.set(a.get())

def Cargar1():
	a.set(guardado1.get())
def Cargar2():
	a.set(guardado2.get())
def Cargar3():
	a.set(guardado3.get())
def Cargar4():
	a.set(guardado4.get())
def Cargar5():
	a.set(guardado5.get())

#Creamos los btnes con sus respectivos nombres
#NUMEROS
btn1=Button(ventana, text="1", width=10, height=2, command=lambda:refreshscreen(1))
btn1.place(x=1, y=375)
btn2=Button(ventana, text="2", width=10, height=2, command=lambda:refreshscreen(2))
btn2.place(x=90, y=375)
btn3=Button(ventana, text="3", width=10, height=2, command=lambda:refreshscreen(3))
btn3.place(x=179, y=375)
btn4=Button(ventana, text="4", width=10, height=2, command=lambda:refreshscreen(4))
btn4.place(x=1, y=317)
btn5=Button(ventana, text="5", width=10, height=2, command=lambda:refreshscreen(5))
btn5.place(x=90, y=317)
btn6=Button(ventana, text="6", width=10, height=2, command=lambda:refreshscreen(6))
btn6.place(x=179, y=317)
btn7=Button(ventana, text="7", width=10, height=2, command=lambda:refreshscreen(7))
btn7.place(x=1, y=259)
btn8=Button(ventana, text="8", width=10, height=2, command=lambda:refreshscreen(8))
btn8.place(x=90, y=259)
btn9=Button(ventana, text="9", width=10, height=2, command=lambda:refreshscreen(9))
btn9.place(x=179, y=259)
btn0=Button(ventana, text="0", width=10, height=2, command=lambda:refreshscreen(0))
btn0.place(x=1, y=433)

#OPERACIONES
btnsuma=Button(ventana, text="+", width=10, height=2, command=lambda:refreshscreen("+"))
btnsuma.place(x=268, y=375)
btnresta=Button(ventana, text="-", width=10, height=2, command=lambda:refreshscreen("-"))
btnresta.place(x=357, y=375)
btnmultip=Button(ventana, text="x", width=10, height=2, command=lambda:refreshscreen("*"))
btnmultip.place(x=268, y=317)
btndivi=Button(ventana, text="÷", width=10, height=2, command=lambda:refreshscreen("/"))
btndivi.place(x=357, y=317)
btnlog=Button(ventana, text="log", width=10, height=2, command=lambda:refreshscreen("log"))
btnlog.place(x=179, y=201)
btneuler=Button(ventana, text="e", width=10, height=2, command=lambda:refreshscreen(2.71))
btneuler.place(x=268, y=143)
btnpi=Button(ventana, text="π", width=10, height=2, command=lambda:refreshscreen(3.14))
btnpi.place(x=357, y=143)
btnporc=Button(ventana, text="%", width=10, height=2, command=lambda:refreshscreen("%"))
btnporc.place(x=268, y=259)
btnsqrt=Button(ventana, text="√", width=10, height=2, command=lambda:refreshscreen("sqrt("))
btnsqrt.place(x=90, y=201)
btnexp=Button(ventana, text="**", width=10, height=2, command=lambda:refreshscreen("**"))
btnexp.place(x=1, y=201)
btnresl=Button(ventana, text="=", width=23, height=2, command=EvaluateOperation)
btnresl.place(x=268, y=433)

#EXTRAS
btnpariz=Button(ventana, text="(", width=10, height=2, command=lambda:refreshscreen("("))
btnpariz.place(x=268, y=201)
btnparde=Button(ventana, text=")", width=10, height=2, command=lambda:refreshscreen(")"))
btnparde.place(x=357, y=201)
btnDEL=Button(ventana, text="DEL", width=10, height=2, command=clearScreenC)
btnDEL.place(x=357, y=259)
btncoma=Button(ventana, text=",", width=10, height=2, command=lambda:refreshscreen("."))
btncoma.place(x=90, y=433)

salida=Entry(ventana, font=("Arial",20,"bold"), textvariable=a, width=22, bd=40, insertwidth=10, bg="white", justify="left")
salida.pack()


barraMenu = Menu(ventana)
menuConversiones = Menu(barraMenu)
menuGuardar = Menu(barraMenu)
menuCargar = Menu(barraMenu)

barraMenu.add_cascade(label="Salir",command = salir)
barraMenu.add_cascade(label="Menu Conversiones",menu = menuConversiones)
barraMenu.add_cascade(label="Guardar Resultado",menu = menuGuardar)
barraMenu.add_cascade(label="Cargar Resultado",menu = menuCargar)

menuConversiones.add_command(label = "Conversion de Volumen",command = ConvVol)
menuConversiones.add_command(label = "Conversion de Longitud",command = ConvLong)
menuConversiones.add_command(label = "Conversion de Temperatura",command = ConvTemp)
menuConversiones.add_command(label = "Conversion de Peso",command = ConvPeso)
menuConversiones.add_command(label = "Conversion de Presion",command = ConvPresion)
menuConversiones.add_separator()

menuGuardar.add_command(label = "Guardar Resultado 1",command = Guardar1)
menuGuardar.add_command(label = "Guardar Resultado 2",command = Guardar2)
menuGuardar.add_command(label = "Guardar Resultado 3",command = Guardar3)
menuGuardar.add_command(label = "Guardar Resultado 4",command = Guardar4)
menuGuardar.add_command(label = "Guardar Resultado 5",command = Guardar5)
menuGuardar.add_separator()

menuCargar.add_command(label = "Cargar Resultado 1",command = Cargar1)
menuCargar.add_command(label = "Cargar Resultado 2",command = Cargar2)
menuCargar.add_command(label = "Cargar Resultado 3",command = Cargar3)
menuCargar.add_command(label = "Cargar Resultado 4",command = Cargar4)
menuCargar.add_command(label = "Cargar Resultado 5",command = Cargar5)
menuCargar.add_separator()

ventana.config(menu=barraMenu)
ventana.mainloop()